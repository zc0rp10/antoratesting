
#Build with Antora
# antora --fetch user-playbook.yml
# antora --fetch partner-playbook.yml
# antora --fetch internal-playbook.yml

#Build with Antora + Lunr
DOCSEARCH_ENABLED=true DOCSEARCH_ENGINE=lunr DOCSEARCH_LANGS=en,sv NODE_PATH="$(npm -g root)" antora --fetch --generator antora-site-generator-lunr user-playbook.yml
DOCSEARCH_ENABLED=true DOCSEARCH_ENGINE=lunr DOCSEARCH_LANGS=en,sv NODE_PATH="$(npm -g root)" antora --fetch --generator antora-site-generator-lunr partner-playbook.yml
DOCSEARCH_ENABLED=true DOCSEARCH_ENGINE=lunr DOCSEARCH_LANGS=en,sv NODE_PATH="$(npm -g root)" antora --fetch --generator antora-site-generator-lunr internal-playbook.yml